import { IBuilder, IBuilderWood } from "./Builder.interface";
import { House } from "./House.product";




export class WoodBuilder implements IBuilder {
    private house: House;

    constructor() {
        this.house = new House();
    }

    public reset() {
        this.house = new House();
    }

    build(): House {
        const result = this.house;
        this.reset();
        return result;
    }

    buildDoor(): void {
        this.house.addPart("Agrega una puerta de madera");
    }

    buildPool(): void {
        this.house.addPart("Agregar una piscina de madera");
    }

    buildWall(): void {
        this.house.addPart("Agrega  una pared de madera");
    }
}


export class WoodBuilderExtended extends WoodBuilder implements IBuilderWood {
    constructor() {
        super();
    }

    buildTerrecent(): void {
        throw new Error("Method not implemented.");
    }
    buildRoof(): void {
        throw new Error("Method not implemented.");
    }
}