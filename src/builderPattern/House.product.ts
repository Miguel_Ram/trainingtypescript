interface IProduct {
    listParts(): void;
    addPart(part: string): void;
}

export class House implements IProduct {
    private parts: string[] = [];

    listParts(): void {
        this.parts.forEach((part, index) => {
            console.log(`${ index }) ${ part }`);
        });
    }

    addPart(part: string): void {
        this.parts.push(part);
    }
}