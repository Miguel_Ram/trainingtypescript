import { IBuilder } from "./Builder.interface";

export class Director {
    private builder: IBuilder;

    public setBuilder(_builder: IBuilder) {
        this.builder = _builder;
    }

    public buildMinimalVersion(): void {
        this.builder.buildWall();
        this.builder.buildWall();
        this.builder.buildWall();
        this.builder.buildWall();

        this.builder.buildDoor();
    }

    public buildDeluxVersion(): void {
        this.builder.buildWall();
        this.builder.buildWall();
        this.builder.buildWall();
        this.builder.buildWall();

        this.builder.buildDoor();
        this.builder.buildDoor();

        this.builder.buildPool();
        this.builder.buildPool();
        this.builder.buildPool();
    }
}