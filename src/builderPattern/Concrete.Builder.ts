import { IBuilder } from "./Builder.interface";
import { House } from "./House.product";

export class ConcreteBuilder implements IBuilder {
    private house: House;

    constructor() {
        this.house = new House();
    }

    public reset() {
        this.house = new House();
    }

    build(): House {
        const result = this.house;
        this.reset();
        return result;
    }

    buildDoor(): void {
        this.house.addPart("Agrega una puerta de concreto");
    }

    buildPool(): void {
        this.house.addPart("Agrega una piscina de concreto");
    }

    buildWall(): void {
        this.house.addPart("Agrega una pared de concreto");
    }
}