import { ConcreteBuilder } from "./Concrete.Builder";
import { Director } from "./Director";
import { WoodBuilder } from "./Wood.Builder";

export class Client {
    public buyAHouse(): void {
        const builder = new WoodBuilder();

        const director = new Director();
        director.setBuilder(builder);
        director.buildDeluxVersion();

        builder.build().listParts();
    }
}