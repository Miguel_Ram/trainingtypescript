export interface IBuilder {
    buildDoor(): void;
    buildPool(): void;
    buildWall(): void;
}

export interface IBuilderWood {
    buildTerrecent(): void;
    buildRoof(): void;
}