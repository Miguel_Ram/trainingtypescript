import { BaseFigure } from "./BaseFigure";

export class Triangle extends BaseFigure {
    private base: number;
    private height: number;

    constructor(base: number, height: number) {
        super();
        this.base = base;
        this.height = height;
    }

    getArea(): void {
        console.log("Area: " + (this.base * this.height) / 2);
    }
}



