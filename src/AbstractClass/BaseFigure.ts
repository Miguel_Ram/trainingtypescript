export abstract class BaseFigure {
    static counterFigure: number = 0;
    protected color: string = "black";

    constructor() {
        BaseFigure.counterFigure += 1;
    }

    get getColor() {
        return this.color;
    }

    set setColor(value: string) {
        this.color = value;
    }

    abstract getArea(): void;
}


BaseFigure.counterFigure;
