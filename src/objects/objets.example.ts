export type Person = {
    name: string;
    lastName: string;
    age: number;
};

export interface PersonGeneric {
    name: string;

}

