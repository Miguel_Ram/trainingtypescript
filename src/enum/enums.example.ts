export enum Roles {
    user,
    medic,
    admin,
    superAdmin,
}