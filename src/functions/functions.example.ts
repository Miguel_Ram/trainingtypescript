import axios, { AxiosResponse } from "axios";
export function buildName(firstName: string, lastName: string, ...restOfName: string[]): string {
    return firstName + " " + lastName + " " + restOfName.join(" ");
}

// interface User {
//     id: number;
//     name: string;
//     address: {
//         lng: string;
//         lat: string;
//     };
// }

//leer el jsonplaceholder
//función debe aceptar un id:string => usuario
async function request(): Promise<User[]> {
    return await axios.get("https://api.twitter.com/");
}

export interface Geo {
    lat: string;
    lng: string;
}

export interface Address {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: Geo;
}

export interface Company {
    name: string;
    catchPhrase: string;
    bs: string;
}

export interface User {
    id: number;
    name: string;
    username: string;
    email: string;
    address: Address;
    phone: string;
    website: string;
    company: Company;
}

export const myfuction = async (id: number): Promise<void> => {
    try {
        let response: AxiosResponse = await axios.get("https://jsonplaceholder.typicode.com/users");
        const users: User[] = response.data;

        const user = users.find(user => user.id === id);
        console.log(user);
    } catch (error) {
        console.log(error);
    }
};
//myfuction(1);