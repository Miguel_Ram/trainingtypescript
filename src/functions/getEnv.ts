type DataType = string | number | boolean;

interface ClassConstructor<T extends DataType> {
    (value: any): T;
}

export function getEnv<T extends DataType>(name: string, cast: ClassConstructor<T>): T {
    console.log(typeof cast);
    return cast(process.env[name]);
};