import { StackOperations } from "./stack";

export interface StackOperations1 {
    push(element: string): void;
    pop(): string | null;
    peek(): string | null; //regresa el elemento sin eliminarlo del stack
    getSize(): number;
    isEmpty(): boolean;
}

export class Stack implements StackOperations1 {
    private elements: string[] = [];

    push(element: string): void {
        this.elements.push(element);
    }

    public peek(): string | null {
        if (this.getSize() === 0) {
            return null;
        }
        return this.elements[this.elements.length - 1];
    }

    pop(): string | null {
        if (this.getSize() === 0) {
            return null;
        }
        return this.elements.pop() as string;
    }

    getSize(): number {
        return this.elements.length;
    }

    isEmpty(): boolean {
        return this.elements.length === 0 ? true : false;
    }
}