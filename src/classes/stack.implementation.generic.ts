interface BaseType {
    name: string;
    age: number;
}

export interface StackOperations1<T> {
    push(element: T): void;
    pop(): T | null;
    peek(): T | null;
    getSize(): number;
    isEmpty(): boolean;
}

export class Stack<V extends BaseType> implements StackOperations1<V> {
    private elements: V[] = [];

    push(element: V): void {
        this.elements.push(element);
    }

    public peek(): V | null {
        if (this.getSize() === 0) {
            return null;
        }
        return this.elements[this.elements.length - 1];
    }

    pop(): V | null {
        if (this.getSize() === 0) {
            return null;
        }
        return this.elements.pop() as V;
    }

    getSize(): number {
        return this.elements.length;
    }

    isEmpty(): boolean {
        return this.elements.length === 0 ? true : false;
    }
}

type User = {
    name: string,
    lastName: string;
    age: number;
};



const stack3 = new Stack<User>();
stack3.push({
    name: "Miguel",
    lastName: "Ramírez",
    age: 25
});