export class Singleton {
    private static instance: Singleton;

    private constructor() { }

    public static getInstance(): Singleton {
        if (!Singleton.instance) {
            console.log("Crea la instancia");
            Singleton.instance = new Singleton();
        }

        console.log("Retorna instancia");
        return Singleton.instance;
    }

    public generateRandomValues(): void {
        console.log("Generating random values: ", "123456");
    }
}


