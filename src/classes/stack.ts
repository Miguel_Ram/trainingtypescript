export interface StackOperations {
    push(element: string): void;
    pop(): string;
    peek(): string; //regresa el elemento sin eliminarlo del stack
    getSize(): number;
    isEmpty(): boolean;
}