import { Person, PersonGeneric } from './src/objects/objets.example';
import { Tuple } from './src/tupla/tupla.example';
import { Roles } from './src/enum/enums.example';
import { buildName, myfuction } from './src/functions/functions.example';
import { Singleton } from './src/classes/Singleton.pattern';
import { Client } from './src/builderPattern/Client';
import { Triangle } from './src/AbstractClass/Triangle';
import { BaseFigure } from './src/AbstractClass/BaseFigure';
import { getEnv } from './src/functions/getEnv';
import dotenv from "dotenv";
dotenv.config();


// function error(message: string): never {
//     throw new Error(message);
// }

// async function getExternalBack(user: string, password: string): Promise<Person> {
//     return await {
//         name: "David",
//         lastName: "Arellano",
//         age: 24
//     };
// }

// type Person = {
//     name: string;
//     lastName: string;
//     age: number;
// };

// interface Adapter<T> {
//     getUsers(): T[];
// }

// interface Human {
//     fullName: string;
//     country: string;
// }

// enum Browser {
//     firefox = "firefox",
//     chrome = "chrome",
//     Opera = "opera"
// }

// type BrowserType = Human | Person;
// function process(browser: Human | Person) {
//     console.log(browser);
// }


// (() => {
//     console.log(Browser.Opera);
//     process({
//         name: "David",
//         lastName: "Arellano",
//         age: 24
//     });

//     const other: string = "sadasd";
//     const name = "Miguel";
//     const age: number = 25;
//     const isDeleted: boolean = true;

//     let names: string[] = [];
//     names.push("Manuel");
//     names.push("Gabo");
//     names.push("Jose");

//     let countries: any = [];
//     countries = "México";

//     let userInput: unknown = 4;
//     userInput = "hi";
//     userInput = false;

//     names = countries;

//     const user: Person = {
//         name: "David",
//         lastName: "Arellano",
//         age: 24
//     };

//     const user2: Human = {
//         fullName: "Jorge",
//         country: "México"
//     };

// }
// )();


(async () => {
    let name = "Erberth";
    const age: number = 25;
    const myBoolean: boolean = false;

    const names: string[] = [];
    //names.push(14)

    let number: number = 0;

    let anyvariable: any = "sdasdasd";
    anyvariable = 12;
    anyvariable = true;

    let userInput: unknown = 4;
    userInput = "sdasds";

    number = anyvariable;
    number = userInput as number;

    let unknownVariable: unknown = "sdasdasd";
    unknownVariable = userInput;

    const myInfo: {
        name: string;
        lastName?: string;
    } = {
        name: 'John'
    };

    const myTuple: Tuple = ["age", 25];

    console.log(Roles.superAdmin);

    //console.log(buildName("Miguel", "Angel", "Ramirez", "Castillo", "Segundo"));


    //await myfuction(100);

    //Singleton.getInstance();
    //Singleton.getInstance();
    //Singleton.getInstance().generateRandomValues();

    //new Client().buyAHouse();
    console.log(BaseFigure.counterFigure);
    const trianguleInstance = new Triangle(4, 5);
    trianguleInstance.getArea();
    console.log(BaseFigure.counterFigure);

    const trianguleInstance2 = new Triangle(7, 5);
    const trianguleInstance3 = new Triangle(7, 5);
    console.log(BaseFigure.counterFigure);

    //función genérica
    const port = getEnv("PORT", Boolean);
    console.log(port);
})();